# XMPP Webhook

A simple webhook to allow HTTP POST requests to be sent to an XMPP chat through a JSON payload.

## Usage

The easiest way to start using the webhook would be through Docker.

```
docker run -p 3000:3000 -d --name xmpp-webhook registry.gitlab.com/logangarcia/xmpp-webhook
```

The webhook has four endpoints:

* `/sendMessage`
* `/sendFile`
* `/sendGroupMessage`
* `/sendGroupFile`

All of the endpoints have the following JSON schema:

```
{
    "jid": "user@domain.tld",
    "password": "some-password",
    "recipient": "recipient@domain.tld",
    "nick": "Nickname",
    "message": "Test message"
}
```

For the `/sendGroupMessage` and `/sendGroupFile` endpoints, the recipient is the JID of the Multi-User Chat. When sending a file, the `message` field is assumed to be a URL to some resource.

To test out the webhook locally:

```
curl -X POST -H "Content-Type: application/json" -d \
    '{"jid": "user@domain.tld", "password": "some-password", \
    "recipient": "recipient@domain.tld", "nick": "Nickname", \
    "message": "https://domain.com/resources/image.jpg"}' \
    http://localhost:3000/sendFile
```

## License

[Apache License 2.0](https://choosealicense.com/licenses/apache-2.0/)
