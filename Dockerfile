FROM python:3.9 AS builder

COPY requirements.txt .

RUN pip install --user -r requirements.txt

FROM python:3.9-slim

WORKDIR /app

COPY --from=builder /root/.local /root/.local
COPY xmpp-webhook.py .

ENV PATH=/root/.local:$PATH

EXPOSE 3000

CMD [ "python", "xmpp-webhook.py"]
