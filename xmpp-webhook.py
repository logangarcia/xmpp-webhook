#!/usr/bin/env python3

# Copyright 2023 Logan Garcia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from bottle import post, request, run

import slixmpp

class XMPPBot(slixmpp.ClientXMPP):
    '''
    A basic slixmpp bot that will log in, send a message,
    and then log out.
    '''
    def __init__(self,
                 jid,
                 password,
                 recipient,
                 nick,
                 message,
                 muc,
                 file):

        slixmpp.ClientXMPP.__init__(self, jid, password)

        self.recipient = recipient
        self.nick = nick
        self.message = message
        self.muc = muc
        self.file = file

        if file:
            self.register_plugin('xep_0066') # OOB
        if muc:
            self.register_plugin('xep_0045') # Multi-User Chat

        # The session_start event will be triggered when
        # the bot establishes its connection with the server
        # and the XML streams are ready for use. We want to
        # listen for this event so that we we can initialize
        # our roster.
        self.add_event_handler("session_start", self.start)

    async def start(self, event):
        self.send_presence()
        await self.get_roster()

        if self.muc:
            self.plugin['xep_0045'].joinMUC(self.recipient,
                                            self.nick)

        m = self.Message()
        m['to'] = self.recipient
        m['type'] = 'groupchat' if self.muc else 'chat'
        m['body'] = self.message

        # When sending a URL file, the URL must be
        # reflected in both the message body as well
        # as the OOB segment
        if self.file:
            m['oob']['url'] = self.message

        m.send()
        self.disconnect()

@post('/sendMessage')
def send_message():
    print('sending message')
    return send_xmpp_message(request, False, False)

@post('/sendFile')
def send_file():
    return send_xmpp_message(request, False, True)

@post('/sendGroupMessage')
def send_group_message():
    return send_xmpp_message(request, True, False)

@post('/sendGroupFile')
def send_group_file():
    return send_xmpp_message(request, True, True)

def send_xmpp_message(request, muc=False, file=False):
    data = request.json

    xmpp = XMPPBot(data.get('jid'),
                   data.get('password'),
                   data.get('recipient'),
                   data.get('nick'),
                   data.get('message'),
                   muc,
                   file)

    xmpp.register_plugin('xep_0030') # Service Discovery
    xmpp.register_plugin('xep_0199') # XMPP Ping

    xmpp.connect()
    xmpp.process(forever=False)

run(host='0.0.0.0', port=3000)
